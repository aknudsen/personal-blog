+++
title = "Achieving Infinite Scrolling with React.js"
date = 2016-09-05T11:11:00+02:00
categories = ["Development", "JavaScript"]
tags = ["development", "javascript", "react"]
+++
As part of developing MuzHack I had to implement a method of loading search results on demand, i.e. not everything at once. I decided on the http://scrollmagic.io/examples/advanced/infinite_scrolling.html["infinite scrolling"^] method, which means that as seen on f.ex. Twitter, you load more content as the user scrolls down the page. This is an alternative to the more traditional pagination method, where content (e.g. search results) is partitioned into pages, and the user loads more content by explicitly navigating to another page.

As I am using a virtual representation of the Document Object Model (AKA DOM) via the React.js JavaScript framework, however, actually getting infinite scrolling to work is easier said than done. This is a type of problem that is much easier to solve if you're working directly with the browser DOM and there's plenty of prior art to look to. I am also using the Masonry framework to lay out search results as tiles in MuzHack, via https://github.com/eiriklv/react-masonry-component[Eirik Langholm Vullum's excellent React component^], so that presented another challenge in getting infinite scrolling to work, as the two technologies would have to work together in harmony.

As it so happens though, Eirik let me know he had already developed an https://github.com/eiriklv/react-infinite-scroll[infinite scrolling component for React^], which could be used together with his Masonry component. This component works in a simpler way than I had foreseen; when the document is scrolled, the component detects whether the height of the remaining content beneath the currently visible content is less than a certain threshold. If the height is indeed less, the component triggers the loading of more content so that there will eventually be more content for the user to scroll to. A very simple and elegant solution!

After https://github.com/aknuds1/react-infinite-scroll[forking^] Eirik's version of the component and making some adjustments to it, I eventually integrated it with the Masonry component and voilà, MuzHack had infinite scrolling of search results!
