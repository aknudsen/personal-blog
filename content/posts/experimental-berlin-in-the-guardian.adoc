+++
title = "Experimental Berlin Mentioned in The Guardian"
date = 2018-03-30T16:52:00+02:00
categories = ["Development"]
tags = ["development", "publicity"]
featured_image = "/images/newspaper.jpg"
+++
My project https://experimental.berlin[Experimental Berlin^] received a nice mention in 
British newspaper 
https://www.theguardian.com/music/2018/mar/28/facebook-experimental-alternative-music-marketing-algorithm[The Guardian^]!
